FROM node:13-buster-slim

# Install web-ext
USER node
WORKDIR /home/node
ENV PATH=/home/node/.yarn/bin:$PATH

RUN yarn global add web-ext@4.0.0
